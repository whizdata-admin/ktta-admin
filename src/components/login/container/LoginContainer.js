import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Box } from "@mui/material";
import "../login.css";
import Login from "../components/Login";
import { db } from "../../../firebase_config";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { collection, query, where, onSnapshot } from "firebase/firestore";

const LoginContainer = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const [show, setShow] = useState(false);

  let navigate = useNavigate();

  useEffect(() => {
    let authToken = sessionStorage.getItem("Auth Token");

    if (authToken) {
      navigate("/");
    }
  }, []);

  const handleLogin = () => {
    setShow(true);
    const authentication = getAuth();
    signInWithEmailAndPassword(authentication, email, password)
      .then((response) => {
        sessionStorage.setItem(
          "Auth Token",
          response._tokenResponse.refreshToken
        );
        try {
          const q = query(collection(db, "users"), where("email", "==", email));
          onSnapshot(q, (querySnapshot) => {
            querySnapshot.forEach((doc) => {
              sessionStorage.setItem(
                "userPath",
                JSON.stringify(doc.data().path)
              );
            });
          });
        } catch (error) {
          console.log(error);
        }
        setSuccess("User verification successful. Redirecting to dashboard.");
        setShow(false);
        setTimeout(() => {
          setSuccess("");
          navigate("/");
        }, 3000);
      })
      .catch((error) => {
        setShow(false);
        if (error.code === "auth/wrong-password") {
          setError("Please check the Password");
          setTimeout(() => {
            setError("");
          }, 2000);
        }
        if (error.code === "auth/user-not-found") {
          setError("Please check the Email");
          setTimeout(() => {
            setError("");
          }, 2000);
        }
      });
  };

  return (
    <Box className='login__container'>
      <Login
        email={email}
        setEmail={setEmail}
        password={password}
        setPassword={setPassword}
        success={success}
        error={error}
        handleLogin={handleLogin}
        show={show}
      />
    </Box>
  );
};

export default LoginContainer;
