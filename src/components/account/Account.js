import Button from "../Common/Button/Button";

const Account = ({ handleLogout }) => {
  return (
    <>
      <Button
        id='logoutButton'
        variant='outlined'
        onClick={handleLogout}
        content='Logout'
      />
    </>
  );
};

export default Account;
