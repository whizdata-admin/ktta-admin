import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyCmxlRQu5cbiPs9hDuGU6uyrZ-T994mtXE",
  authDomain: "ktta-admin.firebaseapp.com",
  projectId: "ktta-admin",
  storageBucket: "ktta-admin.appspot.com",
  messagingSenderId: "975822053649",
  appId: "1:975822053649:web:b0e37dcf74c497504b2cb0",
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
export const db = getFirestore(app);
