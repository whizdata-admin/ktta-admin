import { useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Dashboard from "./components/layout/Dashboard";
import LoginContainer from "./components/login/container/LoginContainer";
import RegisterContainer from "./components/register/container/RegisterContainer";

const App = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Dashboard />} exact strict />
          <Route path='/login' element={<LoginContainer />} exact />
          <Route path='/register' element={<RegisterContainer />} exact />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default App;
